import java.util.*;
import java.util.stream.Collectors;

public class Formel {
	private HashSet<Literal> _literals;
	private HashSet<HashSet<Literal>> _formel;
	Formel (HashSet<Literal>...klausel) {
		_formel = new HashSet<>(Arrays.asList(klausel));
		_literals = new HashSet<>(_formel.stream().flatMap(Collection::stream).collect(Collectors.toSet()));
	}
	Formel (ArrayList<HashSet<Literal>> klausel) {
		_formel = new HashSet<>(klausel);
		_literals = new HashSet<>();
		_literals = new HashSet<>(_formel.stream().flatMap(Collection::stream).collect(Collectors.toSet()));
	}
	private Formel (HashSet<HashSet<Literal>> formel) {
		_formel = formel;
		_literals = new HashSet<>(_formel.stream().flatMap(Collection::stream).collect(Collectors.toSet()));
	}
	public Formel UP(Literal l) {
		return new Formel(new HashSet<>(_formel.stream()
				.filter(x -> !x.contains(l))
				.map(x -> {
					HashSet<Literal> y = (HashSet<Literal>) x.clone();
					y.remove(l.negate());
					return y;
				})
				.collect(Collectors.toSet())));
	}
	public boolean isSolved() {
		return _formel.isEmpty();
	}
	public boolean mayBeSolvable() {
		return _formel.stream().noneMatch(HashSet::isEmpty);
	}
	//This should work now.
	public Literal findNextLiteral() {
		Optional<HashSet<Literal>> OLR = _formel.stream().filter(x -> x.size() == 1).findFirst();
		if (OLR.isPresent()) return OLR.get().iterator().next();

		Optional<Literal> PLR = _literals.stream().filter(x -> !_literals.contains(x.negate())).findFirst();
		if (PLR.isPresent()) return PLR.get();

		return _literals.iterator().next();
	}
	@Override
	public String toString() {
		return "formel: " + _formel.toString();
	}
}
