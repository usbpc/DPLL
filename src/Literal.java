import java.util.*;

public class Literal implements Comparable<Literal>{
	private int _id;
	private static int _maxId = 1;
	private boolean _negated;
	private String _name;
	private static HashMap<String, Integer> _names = new HashMap<>();
	Literal(String name, boolean negated) {
		_negated = negated;
		_name = name;
		if (_names.containsKey(name)) {
			_id = _names.get(name);
		} else {
			_id = _maxId++;
			_names.put(_name, _id);
		}
	}
	public Literal negate() {
		return new Literal(_name, !_negated);
	}
	@Override
	public int hashCode() {
		return (_negated ? -_id : _id);
	}
	@Override
	public boolean equals(Object o) {
		if (o instanceof Literal) {
			Literal l = (Literal) o;
			return l._id == _id && l._negated == _negated;
		}
		return false;
	}
	@Override
	public String toString() {
		return _name + (_negated ? "\\false" : "\\true") ;
	}
	@Override
	public int compareTo(Literal l) {
		return _id - l._id;
	}
}
