import java.util.ArrayList;
import java.util.HashSet;

public class Parser {

	public static Formel parse(String in) {
		ArrayList<HashSet<Literal>> klauseln = new ArrayList<>();
		int klauselnIndex = 0;
		for (int i = 0; i < in.length(); i++) {
			if (in.charAt(i) == '{') {
				klauseln.add(new HashSet<>());
				while (in.charAt(i) != '}') {
					boolean negated = false;
					if (in.charAt(i) == ',' || in.charAt(i) == ' ' || in.charAt(i) == '{') {
						i++;
						continue;
					}
					if (in.charAt(i) == '¬') {
						negated = true;
						i++;
					}
					klauseln.get(klauselnIndex).add(new Literal(in.substring(i, i+1), negated));
					i++;
				}
				klauselnIndex++;
			}
		}
		return new Formel(klauseln);
	}
	public static void main(String...args) {
		String input = "{B, ¬E, ¬D, ¬A}, {B, D, A, ¬E}, {A, ¬E, ¬B, ¬D}, {C, ¬A}, {E, ¬D, ¬A}, {D, ¬E, ¬A}, {E, D, ¬C, ¬A}, {¬E, ¬A}";
		//String input = "{¬E}, {E}";
		System.out.println(Solver.notRecursiveSolve(parse(input)));
	}
}
