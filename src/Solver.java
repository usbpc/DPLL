import java.util.Stack;

public class Solver {
	public static String notRecursiveSolve(Formel f) {
		Stack<Formel> formels = new Stack<>();
		Stack<Literal> literals = new Stack<>();
		while (!f.isSolved()) {
			System.out.println(f.toString());
			if (f.mayBeSolvable()) {
				formels.add(f);
				Literal l = f.findNextLiteral();
				System.out.println("Verwendetes Literal 1: " + l);
				literals.add(l);
				f = f.UP(l);
			} else if (formels.isEmpty()) {
				return "No Solution found.";
			} else {
				f = formels.pop();
				Literal l = f.findNextLiteral().negate();
				System.out.println("Verwendetes Literal 2: " + l);
				literals.pop();
				literals.add(l);
				f = f.UP(l);
			}
		}

		StringBuilder out = new StringBuilder("Solution: ");
		literals.forEach(x -> {
			out.append(x.toString());
			out.append(" ");
		});
		return out.toString();
	}
}
